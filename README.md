# Licenses fonticons

> A webfont with licenses logos

This repository contains the sources used to generate [the fonticon](https://gitlab.com/experimentslabs/licenses-fonticon).

**State of this project:** early days.

## Help appreciated !

Hi there !

Help is always a good thing:

- If you're comfortable with deploying stuff on npmjs, i'd like to build
  a package for easier deployments and usages.
- If you want to create some icons, join the party and make a merge request !
- If you need some other licenses icons, ask it in the issues !

If you have questions or want to chat about this project, join us on [Gitter](https://gitter.im/Experiments-Labs/Lobby)

## Logos:

- [x] Copyright

Most [popular Open Source](https://opensource.org/licenses/category) licenses:
- [x] Apache License 2.0 (Apache-2.0)
- [x] BSD license (BSD)
- [x] GNU General Public License (GPL)
- [x] GNU Lesser General Public License (LGPL)
- [x] MIT license (MIT)
- [x] Mozilla Public License 2.0 (MPL-2.0)
- [ ] Common Development and Distribution License version 1.0 (CDDL-1.0)
- [ ] Eclipse Public License version 2.0

Other
- [ ] [The Unlicense](https://choosealicense.com/licenses/unlicense/)
- [x] [WTFPL](http://www.wtfpl.net/)
- [ ] [Don't be a dick](http://www.dbad-license.org/)
- [x] [Artistic License](https://www.perlfoundation.org/artistic-license-10.html)

Content
- [x] [CC Zero](https://creativecommons.org/publicdomain/zero/1.0/)
- [x] [CC Public domain](https://creativecommons.org/publicdomain/zero/1.0/)
- [x] [CC](https://creativecommons.org/)

## Contributing
The SVG template, `template.svg` can be used as a starting point to design the logos

When contributing a new logo, make sure that:

- it does not overflow the template margins
- it contains only black color on transparent background
- it is cleaned (an online tool as "[SVGOMG](https://jakearchibald.github.io/svgomg/)"
  can be used for now to clean the svg)

## Licenses
All the code is released under the [MIT](LICENSE.md) license.

All the icons are licensed under the [Creative Commons Zero](https://creativecommons.org/publicdomain/zero/1.0/)
license, except for:

- **agpl.svg** was created using [a file from Wikimedia Commons](https://commons.wikimedia.org/wiki/File:AGPLv3_Logo.svg) with unclear license
- **bsd.svg** was created using [a file licensed under the Wikimedia commons Public domain](https://commons.wikimedia.org/wiki/File:License_icon-bsd.svg)
- **cc.svg**, **cc-0.svg**, **cc-pd.svg** were downloaded from the
  [Creative commons website](https://creativecommons.org/about/downloads/).
  I guess they are at least under a Creative Commons license.
- **gpl.svg** was created using [a file from Wikimedia Commons](https://commons.wikimedia.org/wiki/File:GPLv3_Logo.svg) with unclear license
- **lgpl.svg** was created using [a file from Wikimedia Commons](https://commons.wikimedia.org/wiki/File:LGPLv3_Logo.svg) with unclear license
- **mit.svg** was created using [the MIT logo](https://web.mit.edu/). If you're from the MIT, please tell me if I can keep it.
- **mpl.svg** was created using [the Mozilla logo](https://commons.wikimedia.org/wiki/File:Mozilla_logo.svg) with unclear license
- **wtfpl.svg** is the WTFPL logo, under the [WTFPL license](http://www.wtfpl.net/)

**NOTE**: All brand icons are trademarks of their respective owners. The use of
          these trademarks does not indicate endorsement of the trademark holder
          by Font Awesome, nor vice versa. Please do not use brand logos for any
          purpose except to represent the company, product, or service to which
          they refer.

If you represent any of the organization for which a logo has been created
in this font and have any legal issue with it, please contact us by opening
an issue here. We'll be glad to find a good decision together.
You can also contact me directly on [Gitter](https://gitter.im/mtancoigne)

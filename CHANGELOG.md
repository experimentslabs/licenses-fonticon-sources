# Change Log
All notable changes to this project will be documented in this file.


## [0.0.1] - 2018-10-13 - Initial
### Added

Base file organization;

First icons:

- AGPL (`agpl`)
- ApacheLicense (`apache`)
- BSD (`bsd`)
- Creative Commons 0 (`cc-0`)
- Creative Commons Public Domain (`cc-pd`)
- Creative Commons (`cc`)
- Generic Copyright (`copyright`)
- GPL (`gpl`)
- LGPL (`lgpl`)
- MIT (`mit`)
- Mozilla Public License (`mpl`)
- What The Fuck Public License (`wtfpl`)

A blank character determining the glyphs sizes:
- whitespace (`whitespace`)

